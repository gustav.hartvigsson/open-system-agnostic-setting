## Lizard Folk (Generic term for several species)

Found in the tropical, and subtropical climates, lizard folk are cold blooded,
and look like slightly-larger-than-a-human-sized bipedal lizard. Depending on
where they are born they have different colourations and adaptations to work in
that environment. Some have sand coloured skin to work blend in with sand dunes
of a great dessert, others are green to blend in with marches and jungles,
whilst others are bright red because they their culture have a preference for
that colour.

Often described as having no emotions, this is false. They do have emotions,
they just can't express them like other creatures; They don't have the muscles
in their face to express emotions that way, and the tail is not in directly
controlled by their emotions as dogs and wolves.
