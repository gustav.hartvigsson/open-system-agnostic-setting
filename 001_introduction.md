**Licence:** Public Domain

Hail traveller!

It seems you have been hit by Lorry-kun when you were out having a walk, or
perhaps when you were out driving..? I don't know.

Anyway, you are now dead.

Don't panic! Calm down. You are about to reincarnate into a new world that is
yours for the taking, if you can manage to, that is.

Before you are squeezed out of the nether parts of a creature of your new world
I thought I would give you an overview of what you can expect to find in it.
