## Dragons and Their Kin

There are several different kinds of dragons, from True Dragons and Wyvern, to
Drakes and Wyrms. Among these only a few are considered higher beings. All
dragon kin can be found in both metallic and chromatic types.

### Dragons (True Dragons)

Dragons are big lizard-esq things, with six limbs, four legs to stand on, and
two wings. They are not really lizards, but their own thing. They are the most
powerful mortal creatures you can encounter. Is this section the different types
of dragons will be presented. Most kinds of dragons do built hordes of
out of something, and will protect their horde.

Not all dragons you will find you can talk to, and are right out feral, this
irrespective if they are chromatic or metallic in nature.

**Metallic Dragons** are called as such because their scales shine like
different kinds of metal. Most metallic dragons are not feral, and can be talked
to, if they would allow it. Metallic dragons generally form monogamous pairings
that will last till death do them part.

Among these you will find the **Gold Dragons** who are the most
rare of all dragons, and are seen as protectors of the world. They will try to
solve conflicts through diplomacy, but help you gods, if you every anger one.

**Silver Dragons** are the second least common dragon type you will find. They
are seen as arbiters, and will try to keep the peace in their domain, at any
cost. Remember that.

**Copper Dragons** are mischievous in nature, and will roam their domain trying
to find anything to entertain them. This does not mean they can be played with,
as they have a higher chance of being feral in nature.

**Steel Dragons** or **Iron Dragons** are the only type of dragons that can live
in groups, and do not maintain a fixed domain, but can travel around like
nomads. It is not uncommon for them to seek out companionship with other
higher creatures, and are thus often seen with a _dragon rider_ on their back
when travelling. (**Note:** this dose not mean that individual dragons of other
types can't be found with dragon riders, or seeking companionship.) They are
the only type of metallic dragons that do not build hordes, and instead value
companionship and family over riches.

**Chromatic Dragons** are dragons that have 
