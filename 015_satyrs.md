## Satyrs

Satyrs are creatures originally from the Land of Fay, who can not return. They
are stout human-esq creates that have donkey ears and donkey tails. Lives in
farming villages on the surfaces, they are happy-go-lucky in nature, and are
very welcoming of others (You may visit, but never stay for too long). All 
satyrs have light skin, green eyes, and red hair. (They have no horns.)

They will protect their own at any price, but will welcome a wary traveller in
for a cup of tea or for trade in goods.

It is not uncommon to see Satyr merchants in towns and cities of other races,
but will never live there for long. Those that do are often outcasts.
