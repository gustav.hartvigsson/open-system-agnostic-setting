## Dwarfs

Not just short humans or elves. Likes to dig holes. Sometimes dig to deep. Will
never reach the moons..? They live mostly in mountains, where they have dug out
giant fortresses from the stone. They live very communal and share most
resources with each other in each fort.

Excellent masons and engineers, known for their metallurgical know-how, and
protectionist stance on what the have created, and what they produce. The lions
share of metal is dug up by dwarfs, and almost half of all metal refining
is done by the dwarfs.

They are at contrast war with other lesser creatures of the underworld. And are
thus very xenophobic towards them. They have little trust towards elves.
