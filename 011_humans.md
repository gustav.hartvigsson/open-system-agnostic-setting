## Humans

WOO! Humans. Humans are just like you were before you became unalive. Nothing
special.

Humans greatest strength is that they can do almost everything other higher
creatures can. Be it magic, technical or mechanical expertise, or leading
armies. They can do everything (but perhaps are not the best at it).

You will find humans from the frigid north and south, to the deepest deserts and
forests. They build the biggest cities and reproduce like rabbits (according to
other races).

Humans are known to be both extremely xenophobic and extremely xenophilic. They
are known to be very distrusting of other races, and at the same time have an
exceptionally good relations with others (if you know what I mean).

Different gods and different ideologies have their own sets of rules when it
come to non-human sentient creatures.
