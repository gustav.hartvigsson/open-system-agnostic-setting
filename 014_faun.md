## Faun

Faun come in two different variants: Fay Faun, and Devil Faun. Faun are bipedal
creatures with goat-like legs and ram-like horns, on a generic human-like body.

### Fay Faun (true Faun)

Fay Faun are the original faun. They came to this world from the Land of Fay or
the Land of Faeries and were unable to return. They often have the range of skin
colours any human can have, but some can be blessed with lightly blue or green
skin.

They can have long thin tails, often with a wispy tuft of hair on the end,
a bit like a donkeys tail, but longer and more dexterous

They live in arboreal forests where the link to the Land of Fay is as it's
strangest, but are known to also be found in other places. They have taken on 
the role of protectors of the forests, and will try and persuade those who
trespass to leave and never return.

Quote-unquote civilised faun can be found living in or adjacent to human or
other settlements, villages and towns.

### Devil Faun

Devil faun are faun that are the result of interbreeding between Fay Faun and 
Devils or Demons though magic. They are often mistrusted among most other higher
creatures due to this. They can have the full range of skin colour that humans
have, but with a hint of red or yellow. They might also have red or yellow skin.

They can have either a thin tail like a normal Faun, or a thick tail like a
devil.

They are said to be impervious to flames, but it may or may not be true
(Depending on when you are born), better avoid flames just to be sure.

You will find them often living underground in colonies, where try to keep to
themselves. You can also find them in towns and settlements on the surface,
where they often live as entertainers (if you know what I mean) among other
things.
