#!/usr/bin/env bash

___ARGS=$@
___DOCUMENT_TYPE=""
___OUT_FILE="Open System Agnostic Setting"

function print_help() {
  echo "----------"
  echo "build.sh - Build script for this project."
  echo ""
  echo "USAGE:"
  echo "    ./build.sh <type>"
  echo ""
  echo "    Supported types: pdf, rtf, html, epub."
  echo "----------"
}

echoerr() { echo "$@" 1>&2; }

function build_pdf() {
  echo " Building PDF..."
  local _OUT_FILE=$___OUT_FILE.pdf
  local _FILE_LIST=`find . -type f -regextype egrep -iregex './[0-9]{1,3}_.*.md' | sort`
  pandoc $_FILE_LIST \
         -o "$_OUT_FILE"
  echo " done."
}

function build_rtf() {
  echo " Building RFT..."
  local _OUT_FILE=$___OUT_FILE.rtf
  local _FILE_LIST=`find . -type f -regextype egrep -iregex './[0-9]{1,3}_.*.md' | sort`
  pandoc -s -t rst \
         $_FILE_LIST \
        -o "$_OUT_FILE"
  echo " done."
}

function build_html() {
  echo " Building HTML..."
  local _OUT_FILE=$___OUT_FILE.html
  local _FILE_LIST=`find . -type f -regextype egrep -iregex './[0-9]{1,3}_.*.md' | sort`
  pandoc -t html \
         --toc --toc-depth=2 \
         --number-sections  $_FILE_LIST \
         -o "$_OUT_FILE"
  echo " done."
}

function build_epub() {
  echo " Building EPUB..."
  local _OUT_FILE=$___OUT_FILE.epub
  local _FILE_LIST=`find . -type f -regextype egrep -iregex './[0-9]{1,3}_.*.md' | sort`
  pandoc -t epub \
         --toc --toc-depth=2 \
         --number-sections  $_FILE_LIST \
         -o "$_OUT_FILE"
  echo " done."
}

function process_arguments() {
  if [[ $# < 1 ]]; then
    echoerr "----------"
    echoerr "Need to provide at least one argument."
    print_help
    exit 1
  fi
  while [[ $# -gt 0 ]]; do
    case $1 in
      pdf|PDF)
      ___DOCUMENT_TYPE="pdf"
      shift;;
      rft|RTF)
      ___DOCUMENT_TYPE="rtf"
      shift;;
      html|HTML)
      ___DOCUMENT_TYPE="html"
      shift;;
      epub|EPUB)
      ___DOCUMENT_TYPE="epub"
      shift;;
      *)
      echoerr "Unknown argument" $1
      print_help
      exit 2
      shift;;
    esac
  done
}

function main() {
  process_arguments $___ARGS
  
  case $___DOCUMENT_TYPE in
    "pdf")
      build_pdf
    ;;
    "rtf")
      build_rtf
    ;;
    "html")
      build_html
    ;;
    "epub")
      build_epub
    ;;
  esac
}

main
