## Elves

Elves are pointy-eared creatures that live a very very long time. they are known
for their beauty (Well, most of them are, and we don't talk about where the ugly
ones go). There are two main races of Elves: above-ground elves, and below
ground elves.

Depending on when you decide to be born, the split may not have happened yet.
(More on that when we come to it).

Elves generally age as normal humans till they are about the age of 14, when
their aging seems to slow down so it seems it stop to normal humans. The length
of their lives depend on what sort of elf they are.

Interbreeding between the elven races is not unheard of, but not very common.
(Depending on when and where you are born.)

### Above Ground Elves

Above ground elves have three factions: The High Elves, the Tree or Forest
Elves, and the Sea Elves.

**The High Elves** describe themselves as the original elves (or so they say). 
They are the ones blessed by the Gods (or so they say) and have magical power in
abundance. They live in big cities built around trees of power that gives them
long lives and eternal youth.

It is not uncommon for a High Elf to live in excess of a thousand years, but no
more than 5 000 years. There are a few individuals who live forever, either as
punishment or as a blessing from the gods (most often it's punishment).

**The Tree Elves** or **Forest Elves** live in harmony with nature (or so they
say), and have a distrust most other higher creatures, and even other races of
elf. If you are one, you get a free pass on being racist and speciesist. If you
are not one and meet one, expect the worst, but don't be surprised if they are
friendly, as they can be very friendly.

**The Sea Elves** often live on islands far from the main continent. They
generally have darker skin than their land cousins. They are often very
welcoming of strangers, and try to make everyone feel at home. It is not
uncommon that pirates are sea elves.

You may even find sea elves that have become at home _in_ the oceans, with gills
and webbed digits on feet and hands. (Trust at your own risk).

### Below Ground Elves

There are two kinds of Black and White. These two races of elf come to be
because the gods saw a sect of elves as going from the ways of what they
perceived as the path they had set out for elf-kind. They indulged in
depravity, elf sacrifice and torture of other elves en mass.

These two races of elf do not like each other, and fight wars of extermination
and dominance over each other. Other factions of elf have been known to be roped
into their conflict on either side.

**Black Elves** have charcoal black skin, and light eye colours (green, blue,
grey, yellow) and never red or brown. Their natural hair is silver or white.

Black elves can walk the surface without hindrance, and you may find a
black elf or a family of black elves living among other higher creatures, (but
not other elves races).

**White Elves** got the short end of the stick in being outcast. They have pure 
white skin, almost transparent, red eyes, white hair.

They can not survive in direct sunlight and when they do travel the surface they
will ware layers on layers of cloth, and only have a slit to see through.

The white elves are often seen as repentant of what their ancestors have done to
cast their whole linage under ground, without the ability to freely walk the
surface. But that does not mean they can be trusted outright. Depending on when
you are born they might be as evil as they were when they were cast down or even
have purged the evil elements of their ranks and seek to be let back into the
fold of the other races of elf.
