# Higher Creatures

In this section you will learn what sapient creatures exist in this world. You
will be squeezed out of one of these when you are reborn, so it's good to get to
know them.
